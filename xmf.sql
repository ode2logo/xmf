-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 30, 2017 at 01:23 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `xmf`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `lastpos`
-- 

CREATE TABLE `lastpos` (
  `id` int(4) NOT NULL auto_increment,
  `user` varchar(32) default NULL,
  `idei` varchar(24) NOT NULL default '0',
  `lat` varchar(12) NOT NULL default '0',
  `lon` varchar(12) NOT NULL default '0',
  `heading` varchar(6) NOT NULL default '0',
  `sexo` char(1) NOT NULL default 'a',
  `puntos` int(3) NOT NULL default '100',
  `lastup` int(4) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user` (`user`),
  KEY `lastup` (`lastup`),
  KEY `idei` (`idei`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `lastpos`
-- 

INSERT INTO `lastpos` VALUES (1, 'ode', '17747536', '-27.39517904', '-415.9640175', '0', 'o', 13330, 1490846947);
